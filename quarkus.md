### Começando projeto Quakus

```
mvn io.quarkus:quarkus-maven-plugin:1.8.1.Final:create \
   -DprojectGroupId=br.com.franzim.projects \
   -DprojectArtifactId=ff-projects \
   -DclassName="br.com.franzim.projects.quote.api.QuoteResource" \-Dpath="/quotes"
```

cd ff-projetos

Começando projeto Quakus - Gradle

```bash
mvn io.quarkus.platform:quarkus-maven-plugin:2.5.2.Final:create \
    -DprojectGroupId=br.com.franzim.mocks \
    -DprojectArtifactId=ff-mocks \
    -DprojectVersion=0.0.1-SNAPSHOT \
    -DclassName="br.com.franzim.mocks.pinning_certs.VerbsHTTP" \
    -Dextensions="resteasy,kotlin,resteasy-jackson" \
    -DbuildTool=gradle
```



Rodar o banco de dados

```shell
$ docker run --name postgres-quotes -e  "POSTGRES_PASSWORD=postgres" -p 5432:5432 -v ~/developer/PostgreSQL:/var/lib/postgresql/data -d postgres

$ docker start efe1bc402aaa

$ docker start postgres-fii
```

Parar banco de dados

```shell
$ docker stop postgres-fii
```

Rodar 

```
mvn compile quarkus:dev
```

Rodar em debug

```

```

########################



```shell
mvn io.quarkus:quarkus-maven-plugin:1.8.3.Final:create \
    -DprojectGroupId=br.com.franzim \
    -DprojectArtifactId=quotes-project \
    -DclassName="br.com.franzim.quote.api.QuoteResource" \
    -Dpath="/quotes"
cd quotes
```

O Quarkus disponibiliza um site chamado [Quarkus.code.io](https://code.quarkus.io/), onde é posísvel configurar o projeto de uma forma mais visual, vale a pena conferir, segue o link: https://code.quarkus.io/

**Projeto Fiis config Heroku**

DATABASE_URL=postgres://okqknmakshoqtf:0d93400f64af326bc6c324d7425064a8fdf65558fd63bbe1eb6e93aaa37472d8@ec2-52-22-161-59.compute-1.amazonaws.com:5432/dci54tdoebk2r5;DATABASE_PORT=5432;DATABASE_PASSWD=0d93400f64af326bc6c324d7425064a8fdf65558fd63bbe1eb6e93aaa37472d8;DATABASE_USER=okqknmakshoqtf;DATABASE_HOST=ec2-52-22-161-59.compute-1.amazonaws.com;DATABASE_NAME=dci54tdoebk2r5

**Projeto Fiis config Local**

DATABASE_PORT=5432;DATABASE_PASSWD=postgres;DATABASE_USER=postgres;QUARKUS_PROFILE=dev;DATABASE_HOST=127.0.0.1;DATABASE_NAME=postgres_fiis

