**1 - Abrir Conta Via deb;**

1. Acessar o MAI
2. Acessar desenv
3. Opção 12
4. Acessar o DEB
5. Opção 3 "Utilitários" > 01 Subrotinas DEB > Abertura de Conta > Selecionar com S
   1. 2 Jurídica
   2. Colocar o MCI **280908429**
   3. Apertar F9 ou Shift + F9
      4. Razão **310059807**
   5. Num. Especial **89994/** 89993 - 56089
   6. 
   7. **Não é necessário alterar nenhum outro campo**
   8. Apertar enter
   9. Independente da mensagem tentar logar
   10. Caso não funcione tentar a opção 2 (KKK)

2 - Via KKK

1. Acessar o MAI
2. escrever na linha "Command" a instrução: "L DSGRIH11" e apertar enter
3. Colocar a senha
4. Apertar F3
5. Ir na ultima linha das opções do MAI, existirá uma opção semelhante a : "$APPL1 <-> COMPLEXO GRI H - AGENCIA - MEMBRO GHD1"
6. Escrever U no lado esquerdo dessa opção
7. Alterar o valor da opção "Logon request" para DSGRIH11 &USERID/&USERPW/F
8. Colocar a senha
9. Entrar na opção criada
10. Na opção escrever "kkk"
11. Apertar enter
12. Preencher
    1. Opção 1
    2. Agência 7988
    3. Conta 89994
    4. Informação 1: 10
    5. Informação 1: 666666
    6. Apertar enter
13. Preencher
    1. Opção 4
    2. Agência 7988
    3. Conta 89994
    4. Informação 1: 10
    5. Informação 1: 88888888
    6. Apertar enter