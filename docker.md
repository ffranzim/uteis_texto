## Instalação de docker



### Erro Rede

A instalação default do docker tem um conflito de erro com a rede do banco, para alterar a faixa de rede configure o arquivo **/etc/docker/daemon.json** com o conteudo abaixo:

## Docker
Para configurar ip diferente do banco edite o arquivo /etc/docker/daemon.json com o conteudo abaixo:
```json
{
  "bip": "192.168.99.1/24"
}
//NOVO para docker compose
{
    "bip": "192.168.130.0/24",
    "default-gateway": "192.168.130.1"
}
network_mode: bridge

```

https://github.com/docker/toolbox/issues/630

docker-machine create --driver virtualbox --virtualbox-hostonly-cidr "192.168.123.99/24" mymachine

### Comandos

| Comando                                                      | Descricao                                                    | Exemplo                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| docker pull                                                  | Baixa a Imagem                                               | docker pull ubuntu                                           |
| docker images                                                | Lista as imagens                                             | docker images                                                |
| docker run -it                                               | Roda a imagem e deixa o terminal do linux da imagem          | docker run -it ubuntu                                        |
| docker ps                                                    | Lista a imagens em execução                                  | docker ps                                                    |
| docker ps -a                                                 | Mostra todos os containers inclusive os que não estão executando |                                                              |
| docker rm id                                                 | Remove a imagem do docker com o id especificado              |                                                              |
| docker ps -qa                                                | lista os id's de todos os containers                         |                                                              |
| docker rm $(docker ps -qa)                                   | remove todos os containers, só remove os containers que não estão em execução |                                                              |
| docker version                                               | auto explicativo                                             |                                                              |
| docker start id                                              | Sobe a imagem com o id apontado                              |                                                              |
| docker stop id                                               | Mata a imagem com o id apontado                              |                                                              |
| docker start -ai fb70580633d7                                | Sobe a imagem com o id apontado atachado e de maneira iteratica(com console) |                                                              |
| docker container prune                                       | Limpa todos os containers inativos                           |                                                              |
| docker images                                                | Mostra as minhas imagens                                     |                                                              |
| docker rmi nome_da_imagem                                    | Remove o a imagem com nome da imagem                         | docker rmi hello-world                                       |
| docker run -d imagem                                         | O **-d** faz rodar a imagem em background                    |                                                              |
| docker run -d -P imagem                                      | O **-P** atribui uma porta aleatoria apontando para uma posta interna do docker |                                                              |
| docker port imagem                                           | Mostra as portas atribuidas na imagem (um de para)           |                                                              |
| docker run --name nome_atribuido -d -P imagem                | O **--name** da um nome a imagem e você pode trabalhar como se fosse o id |                                                              |
| docker run -d -p 12345:80 imagem                             | O **-p 12345:80** mapeia a porta 80 da imagem na porta 12345 |                                                              |
| docker run --name ff-site -d -p 12345:80 -e AUTHOR=FFRANZIM dockersamples/static-site | O **-e AUTHOR=FFRANZIM** seta uma variavel de ambiente dentro do container |                                                              |
| docker ps -q                                                 | Retorna o id de **todos** os containers ativos               |                                                              |
| docker stop $(docker ps -q)                                  | Mata **todos** os containers ativos                          |                                                              |
| docker run -v "/var/www" imagem<br /><br />docker run -v "path_docker_host:/var/www" imagem | O **-v "/var/www"** cria um volume neste endereço na imagem que está vinculado a um diretorio da máquina fornecedora<br /><br />**-v "/opt/desenv/workspace/docker_curso/volume1:/var/www"** antes dos **:** define o local na maquina origem, senão colocar o docker cria um aleatorio | docker run -v "/opt/desenv/workspace/docker_curso/volume1:/var/www" ubuntu<br /><br />docker run -p 8080:3000 -v "/opt/desenv/workspace/docker_curso/volume-exemplo:/var/www" node npm start |
| docker inspect  id_da_imagem                                 | Dá informações sobre a imagem                                |                                                              |
| Docker Host                                                  | Quem provem o docker                                         |                                                              |
| docker run  -p 8080:3000 -v "$(pwd):/var/www" -w "/var/www"  node:8.2.1 npm start | **-d** pra não vincular ao terminal<br /><br />**-p** pra vincular a porta <br /><br />**-w** pra iniciar na pasta<br /><br />**node:8.2.1** é a imagem<br /><br />**npm start** é comando que rodará na pasta **-w**<br /><br />**$(pwd)** pasta atual --> Lembrar que tem que estar no drietorio | docker run -d  -p 8080:3000 -v "/opt/desenv/workspace/docker_curso/volume-exemplo:/var/www" -w "/var/www"  node:8.2.1 npm start<br /><br />docker run -d -p 8080:3000 -v "$(pwd):/var/www" -w "/var/www"  node:8.2.1 npm start |
| docker build -f nome_do_arquivo<br />-t tag_nome .           | buildar (se o arquivo tiver o nome Dockerfile não é necessário o mesmo **-f**)<br /><br />**-t tagnome** nome que a imagem terá <br /><br />**.** path da imagem | docker build -f node.dockerfile -t ffranzim/node .           |
| docker rm -f id_da_imagem                                    | Remove e força o stop da imagem automicamente                |                                                              |
| docker run -d -p 8080:3000 ffranzim/node                     | Rodando minha imagem                                         |                                                              |
| docker login                                                 | Logar no docker                                              |                                                              |
| docker push imagem                                           | Subir imagem para o docker_hub                               | docker push ffranzim/node                                    |
| docker network create --driver bridge nomeDaRede             | Criando uma rede                                             | docker network create --driver bridge minha-rede             |
| docker run -it --name meu-container-ubuntu --network  minha-rede  ubuntu | Atribuir uma rede a um container                             | docker run -it --name meu-container-ubuntu --network  minha-rede  ubuntu |
| docker network ls                                            | Lista as redes                                               |                                                              |
| docker network inspect idRede                                | Inspeciona a rede                                            |                                                              |
| docker run -d --network minha-rede -p 8080:3000 douglasq/alura-books:cap05 | Sobe imagem mapeando porta e colocando em uma rede especifica |                                                              |
| docker  -d run --name meu-mongo --network minha-rede mongo   | Sobe imagem na rede espeficada                               |                                                              |
| docker system prune                                          | apaga tudo do docker                                         |                                                              |
| docker exec -it alura_books_1 ping alura_books_2             | Executa o comando entre um container e outro                 |                                                              |
| ctrl + p + q                                                 | Sai de um docker iterativo sem matar o container             |                                                              |
| docker history idImagem:versao                               | mostra camadas                                               |                                                              |
| docker info                                                  | Mostra dentre outras coisas se a maquina está no swarm ou não |                                                              |
| docker container rm 1bb --force                              | Derruba container forçadamente                               |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |
|                                                              |                                                              |                                                              |



```


Segue a lista com os principais comandos utilizados durante o curso:

    Comandos relacionados às informações
        docker version - exibe a versão do docker que está instalada.
        docker inspect ID_CONTAINER - retorna diversas informações sobre o container.
        docker ps - exibe todos os containers em execução no momento.
        docker ps -a - exibe todos os containers, independentemente de estarem em execução ou não.

    Comandos relacionados à execução
        docker run NOME_DA_IMAGEM - cria um container com a respectiva imagem passada como parâmetro.
        docker run -it NOME_DA_IMAGEM - conecta o terminal que estamos utilizando com o do container.
        docker run -d -P --name NOME dockersamples/static-site - ao executar, dá um nome ao container e define uma porta aleatória.
        docker run -d -p 12345:80 dockersamples/static-site - define uma porta específica para ser atribuída à porta 80 do container, neste caso 12345.
        docker run -v "CAMINHO_VOLUME" NOME_DA_IMAGEM - cria um volume no respectivo caminho do container.
        docker run -it --name NOME_CONTAINER --network NOME_DA_REDE NOME_IMAGEM - cria um container especificando seu nome e qual rede deverá ser usada.

    Comandos relacionados à inicialização/interrupção
        docker start ID_CONTAINER - inicia o container com id em questão.
        docker start -a -i ID_CONTAINER - inicia o container com id em questão e integra os terminais, além de permitir interação entre ambos.
        docker stop ID_CONTAINER - interrompe o container com id em questão.

    Comandos relacionados à remoção
        docker rm ID_CONTAINER - remove o container com id em questão.
        docker container prune - remove todos os containers que estão parados.
        docker rmi NOME_DA_IMAGEM - remove a imagem passada como parâmetro.

    Comandos relacionados à construção de Dockerfile
        docker build -f Dockerfile - cria uma imagem a partir de um Dockerfile.
        docker build -f Dockerfile -t NOME_USUARIO/NOME_IMAGEM - constrói e nomeia uma imagem não-oficial.
        docker build -f Dockerfile -t NOME_USUARIO/NOME_IMAGEM CAMINHO_DOCKERFILE - constrói e nomeia uma imagem não-oficial informando o caminho para o Dockerfile.

    Comandos relacionados ao Docker Hub
        docker login - inicia o processo de login no Docker Hub.
        docker push NOME_USUARIO/NOME_IMAGEM - envia a imagem criada para o Docker Hub.
        docker pull NOME_USUARIO/NOME_IMAGEM - baixa a imagem desejada do Docker Hub.

    Comandos relacionados à rede
        hostname -i - mostra o ip atribuído ao container pelo docker (funciona apenas dentro do container).
        docker network create --driver bridge NOME_DA_REDE - cria uma rede especificando o driver desejado.

    Comandos relacionados ao docker-compose
        docker-compose build - Realiza o build dos serviços relacionados ao arquivo docker-compose.yml, assim como verifica a sua sintaxe.
        docker-compose up - Sobe todos os containers relacionados ao docker-compose, desde que o build já tenha sido executado.
        docker-compose down - Para todos os serviços em execução que estejam relacionados ao arquivo docker-compose.yml.


```



