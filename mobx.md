## MobX

#### Comandos Uteis

```
1. MobX fica "assistindo" o prjoeto e gera arquivo g. automaticamente **-->** flutter pub run build_runner watch
2. MobX fica "assistindo" o projeto e gera arquivo g. automaticamente e apaga arquivos antigos **-->** flutter packages pub run build_runner
watch --delete-conflicting-outputs
```

#### Coisas Legais

```
1. Usar ModularState em vez do State, já traz seu Controller declarado por default **-->** class _HomePageState extends ModularState<HomePage, HomeController>
```