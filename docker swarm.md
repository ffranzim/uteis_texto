### Docker Swarm

Um orquestrador de docker em varias máquinas, mantem os servicos(mata o antigo e cria um novo)



| Comando                                                      | Exemplo                                                      | Descricao                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| docker-machine ssh idVM                                      | docker-machine ssh vm1                                       | entra na vm                                                  |
| docker-machine ls                                            |                                                              | lista maquinas                                               |
| docker-machine create --driver virtualbox --virtualbox-hostonly-cidr "192.168.123.99/24" vm1 |                                                              | cria máquina                                                 |
| docker-machine create -d virtualbox vm2                      |                                                              | cria maquina                                                 |
| docker-machine start $(docker-machine ls -q)                 | Sobe todas  as máquinas                                      |                                                              |
| docker node inspect vm3 --pretty                             | Exibe as informações de maneira mais facil de ler(bonitinho, parsea o json) |                                                              |
|                                                              |                                                              |                                                              |
| docker swarm init --advertise-addr ipVM                      | docker swarm init --advertise-addr 192.168.123.102           | inicia swarm(cluster)                                        |
| docker swarm init --force-new-cluster --advertise-addr ipVM  |                                                              | Inicia o swarm carregando as  configurações encontras na pasta /var/lib/docker/swarm/ (backup) |
| docker swarm join --token SWMTKN-1-1abpm6a1u3nogp7owfkcbten4dzvu92yqykl2d955eaqkhp27p-ez9avww2iyt8cb41fjhod3uxy 192.168.123.103:2377 | No "pai" dar o comando  docker swarm join-token worker       | Adicionar nó                                                 |
| docker node ls                                               | Lista nos que fazem parte do swarm                           |                                                              |
| docker swarm leave                                           | feito no nó                                                  | baixa o nó do swarm e deixa ele pronto pra ser removido      |
| docker node rm vm3                                           | tira o nó do swarm (precisa que o nó peça pra sair)          |                                                              |
| docker swarm join-token worker                               | Pega o token de adição de um nó                              |                                                              |
| docker swarm join-token manager                              | Pega o token de adição de um manager                         |                                                              |
| docker node inspect vm2                                      | Inspeciona nó                                                |                                                              |
| docker service create -p 8080:3000 aluracursos/barbearia     | Cria um container no escopo do swarm                         |                                                              |
| docker service ls                                            | Lista os serviços                                            |                                                              |
| docker service ps idService                                  | Dá mais informações sobre o serviço, inclusive onde ele esta sendo executado |                                                              |
| /var/lib/docker/swarm/docker swarm join-token manager        | Todo o conetudo o swarm fica amazenado aqui                  |                                                              |
| docker node ls --format "{{.Hostname}} {{.ManagerStatus}}"   | Formatar resposta do ls                                      |                                                              |
| docker node demote nomeVm                                    | Rebaixa de Manager para Worker                               |                                                              |
| docker service ps idService --format "{{.Node}}"             | Lista os nos.                                                | docker service ps hz --format "{{.Node}}"                    |
| docker service rm $(docker service ls -q)                    | Remove todos os serviços                                     |                                                              |
| docker node update --availability drain nomeNo               | Coloca no estado de Drain, não disponivel para nenhum serviço(pra tudo), restringe o nó pra tudo |                                                              |
| docker node update --availability active nomeNo              | Coloca no estado de Active                                   | docker node update --availability active vm4                 |
| docker service update --constraint-add node.role==worker idService | Add restrição que aquele serviço só deve rodar nos nós do tipo worker | docker service update --constraint-add node.role==worker 20jpagsjofo1- Se colocar um papel que não existe o serviço não rodara em ludar nenhum |
| docker service update --constraint-add node.id==idNó ci10k3u7q6ti | Adicioando a restrição para apenas um nó pelo idDoNó         | docker service update --constraint-add node.id==t76gee19fjs8 ci10k3u7q6ti |
| docker service update --constraint-add node.hostname==HostName ci10k3u7q6ti | Adicioando a restrição para apenas um nó pelo nomeDoNó       | docker service update --constraint-add node.hostname==vm4 ci10k3u7q6ti |
| docker service update --constraint-rm node.id==t76gee19fjs8 ci10k3u7q6ti | Removendo a restricao pelo idDoNó                            | docker service update --constraint-rm node.id==t76gee19fjs8 ci10k3u7q6ti |
| docker service update --constraint-rm node.hostname==vm4 ci10k3u7q6ti | Removendo a restrição pelo nome do host                      | docker service update --constraint-rm node.hostname==vm4 ci10k3u7q6ti |
| docker service update --replicas 4 idServico                 | Define o numero de replicas que vão rodar                    | docker service update --replicas 4 20jpagsjofo1              |
| docker service scale idServico=5                             | Define um numero de replicas de maneira diferente            | docker service scale ci10k3u7q6ti=5                          |
| docker service create --name servico --replicas 2 alpine sleep 1d | Define nome do serviço e numero de replicas                  | **"--name servico"** define nome do servico<br />**"--replicas 2"** define o numero de replicas <br />**"sleep 1d"** para o serviço rodar um dia sem dormir |
| docker exec -it servico.2.oorlonj0j1sk4349kuhn0jxku sh       | Entrou no serviço(terminal da imagem alpine)                 |                                                              |
| docker service create --name servico --network my_overlay --replicas 2 alpine sleep 1d |                                                              | **"--name servico"** define nome do servico<br />**"--replicas 2"** define o numero de replicas <br />**" --network my_overlay"** coloca o serviço na rede escolhida <br />**"sleep 1d"** para o serviço rodar um dia sem dormir |
| docker stack deploy --compose-file docker-compose.yml nomeDaStack | Roda um docker-compose.yml em um server                      | **"--compose-file docker-compose.yml"** arquivo compose<br />**"nomeDaStack"** nome da stack(obrigatório) |
| docker stack ls                                              | lista a qtd de serviços da pilha                             |                                                              |
| docker stack ps nomeDaPilha                                  | Lista os serviços da pilha                                   |                                                              |
| docker service ls --format "{{.Name}} {{.Replicas}}"         | Lista os serviços formatados                                 |                                                              |
| docker service ps nodeDaStack_nomeDoServiço                  | Para ver o serviço em cima uma stack                         | docker service ps vote_db                                    |
| docker service ps nodeDaStack_nomeDoServiço                  |                                                              | docker service ps vote_vote                                  |
| docker stack rm nomeDaPilha                                  | Derruba pilha                                                | docker stack rm vote                                         |

​	

