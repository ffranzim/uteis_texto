### VSCode Extensions

Firacode

https://github.com/tonsky/FiraCode

```shell
sudo add-apt-repository universe # Adiciona repo
sudo apt install fonts-firacode # Instala fonte
```

Alterar settings.json

```
"editor.fontFamily": "Fira Code",
"editor.fontLigatures": true,
```

