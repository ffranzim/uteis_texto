### Docker-compose



| Comando               | Descricao                                   | Exemplo |
| --------------------- | ------------------------------------------- | ------- |
| - no docker-compose   | Significa que é um array                    |         |
| docker-compose build  | Executa as instruções do docker-compose.yml |         |
| docker-compose up     | Sobe o docker-compose.yml                   |         |
| docker-compose up -d  | sobe desatachado                            |         |
| docker-compose ps     | lista containers sendo executados           |         |
| docker-compose down   | para containers e remove                    |         |
| docker-compose --help |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |
|                       |                                             |         |

