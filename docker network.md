### Docker Network

Comandos de rede

**Driver Brigde** todos os containers conseguem se comunicar no mesmo host

**Driver Host** ??

**Driver Overlay** ?? todos os nós do swarm estão nesta rede, a comunicação entre diferentes nós de maneira altamente segura

**Service Descovery** --> Descobre o serviço pelo nome

 

| Comando                                                  | Exemplo | Descricao                                                    |
| -------------------------------------------------------- | ------- | ------------------------------------------------------------ |
| docker network ls                                        |         |                                                              |
| docker network create -d overlay my_overlay              |         | Cria rede com o drive overlay                                |
| docker network inspect my_overlay                        |         | Inspeciona a rede                                            |
| docker network create -d overlay --attachable my_overlay |         | Atacha o container(local) a rede overlay. Não entendi direito |
|                                                          |         |                                                              |
|                                                          |         |                                                              |

