## Flutter

### Baixar pacotes

```shell
flutter pub get
```

### Mostrar/Sumir  linhas layout no Debug ➜ VS Code

```vscode
>Flutter:Toggle Debug Painting
```

### Mostrar/Não Mostrar tarja de  debug ➜ VS Code

```vscode
>Flutter:Toggle Debug Painting
```

#### Comandos Uteis

```
1. Cria projeto Flutter **-->** flutter create nome_pr/home/franzim/desenv/libs/flutterojeto
2. Baixar dependencias projeto  **-->** flutter pub get
3. Gerar apk  **-->** flutter build apk
4. Rodar projeto **-->** flutter run
5. Instalar projeto **-->** flutter install
6. Limpar antes do build **-->** flutter clean
7. Ver versões atuais comparadas com as mais novas **-->** flutter pub outdated
8. Configura local do sdk android para o flutter **-->** flutter config --android-sdk /path/to/android/sdk
8. Gerar arquivos(tipo pra executar no linux) flutter **-->** flutter create .
```



### FlutterWeb

Para rodar flutter web sem problema com **CORS** utilizei o artigo [Flutter Web: Some Notes](https://medium.com/flutter-community/flutter-web-for-an-enterprise-app-a056fb4e26d1)

### Modular

#### Bind

1. Factory ➜ Gera uma nova instancia da classe

2. Singleton ➜ Pega sempre a mesma instancia, até que esse modulo morra(dispose)

3. LazySingleton ➜ Só instancia no primeiro get, depois mesmo comportamento do singeton

4. Instance ➜ Não tem construtor,    //? Lança o erro
       dev.log(
         'message',
         stackTrace: null,
         error: 'error',
         name: 'name',
       );

   

   ### Dart Developer

   

   ```dart
   
   import 'dart:developer' as dev; 
   
   //? Lança o erro
   dev.log(
     'message',
     stackTrace: null,
     error: 'error',
     name: 'name',
   );
   
   //? Inspeciona a variavel
   dev.inspect(variavel);
   
   //? Para com um "breakpoint" na linha subsequente
   dev.debugger;
   
   //? Para com um "breakpoint" condicional na linha subsequente
   dev.debugger(when: counter > 2);
   
   //? Funciona pra sincronos
   dev.Timeline.startSync('name');
   //? Calcula o tempo que o codigo dentro dessa estrutura demora a carregar
   //? Isso aparece na ferramenta devtools na ➜ Performance ➜ Timeline Events ➜ Pesquise pelo nome
   dev.Timeline.finishSync();
   
   //? Funciona pra assincronos
   final timelineTask = dev.TimelineTask();
   timelineTask.start('name');
   //? Calcula o tempo que o codigo dentro dessa estrutura demora a carregar
   //? Isso aparece na ferramenta devtools na ➜ Performance ➜ Timeline Events ➜ Pesquise pelo nome
   timelineTask.finish();
   ```
