## Slidy

#### Comandos Uteis

```shell
1. Iniciar slidy no "pc" **-->** flutter pub global activate slidy
2. Iniciar slidy no projeto, executar só em projetos "em branco"  **-->**  slidy start
3. Instalar pacote via slidy   **-->** slidy install nome_pacote | slidy install mobx
4. Executar script declarado no pubspec.yaml **-->**  slidy run nome_do_script
5. Help do slidy **-->** slidy generate -h
6. Gerar page com controller **-->** slidy generate page pages/splash
7. Gerar modulo completo(modulo, controller e page) **-->** slidy generate module modules/login -c
8. Gerar wiget(sem controller)  **-->** slidy g w modules/home/components/item -b
```



### Iniciar projeto com Slidy

```shell
slidy start
```

### Criar Modulo Completo

```shell
slidy g m nomeDaPasta/nomeDoModulo -c
slidy generate module nomeDaPasta/nomeDoModulo --complete 
#ex. slidy g m modules/auth -c
```

### Criar Page

```shell
slidy generate page pages/nome
#ex. slidy generate page /modulepages/splash
```

### Instalar Pacote

```shell
slidy install nomeDoPacote
#ex. slidy install rxdart
```

### Executar script 

```shell
slidy run script nomeDoScript
#ex. slidy run script clean
```

### Listar script 

```shell
slidy run
#ex. slidy run 
```

### Dart

### Instalar Pacote

```shell
dart pub add nomeDoPacote
#ex. dart pub add rxdart
```

### Dart