# Referencias UX
> Seguem algumas sugestões de leitura e capacitação.

### No Banco do Brasil: 

| Descrição                            | Link                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| UX BB - Referencias Da UX BB         | [https://uxresources.design/](https://uxresources.design/)   |
| Mensagens BB - Guia de Redação UX BB | [https://mensagens.labbs.com.br/](https://mensagens.labbs.com.br/) |
| Café com UX -  Evento 2019           | [https://cafecomux.labbs.com.br/](https://cafecomux.labbs.com.br/) |
| UX Conf - Evento 2019                | [http://uxconfbb.labbs.com.br/](http://uxconfbb.labbs.com.br/) |

### Guia para Iniciantes:

[UX Resources - Referencias em inglês](https://uxresources.design/)

### Livros:

|      | Nome                                                         | Status   |
| ---- | ------------------------------------------------------------ | -------- |
|      | Information Architecture for the World Wide Web - Louis Rosenfeld |          |
|      | Neuro Web Design: What makes them click                      |          |
|      | Information Architecture for the World Wide Web - Louis Rosenfeld |          |
|      | Neuro Web Design: What makes them click                      |          |
|      | Designing For The Digital Age - Kim Goodwin/Alan Cooper      |          |
|      | The Elements of User Experience - Jesse James Garrett        |          |
|      | Lean UX: Applying Lean Principles to Improve User Experience - Jeff Gothelf |          |
|      | Mobile Usability - Jakob Nielsen                             |          |
|      | Não me faça pensar - Steve Krug                              |          |
|      | As Leis da Simplicidade                                      |          |
|      | Design Emocional - Donald Norman                             |          |
|      | Design do Dia-a-Dia - Donald Norman                          | Baixando |
|      | Mapeamento de Experiências. Um Guia Para Criar Valor por Meio de Jornadas, Blueprints e Diagramas |          |
|      | Introdução e boas práticas em UX Design                      |          |
|      | Google sprint                                                | Baixando |



####Blogs e Sites:

https://www.nngroup.com/articles/

https://coletivoux.com/

https://brasil.uxdesign.cc/

https://uxdesign.blog.br

https://brasil.uxdesign.cc/

https://coletivoux.com/?gi=a3ff1ebc9d3d

http://catarinasdesign.com.br/blog/



https://www.youtube.com/channel/UCgfaifzmqadwKyCd0lagylQ

https://www.youtube.com/channel/UCw-CQYphBIs_nplLNJuz44w

https://www.youtube.com/channel/UC0f9NbKjUUbnL2cOa12ENIQ



https://itunes.apple.com/us/podcast/uxlx-user-experience-lisbon/id446366947?mt=2

https://uie.fm/

https://uxpod.com/

https://itunes.apple.com/us/podcast/ux-cafe/id374362662?mt=2



####Profissionais:

https://twitter.com/fabriciot

https://medium.com/@elisavolpato

https://twitter.com/memo

https://twitter.com/lukew

https://medium.com/nossa-coletividad/33-pessoas-de-ux-para-seguir-no-twitter-e-no-medium-bc1783cadd8d



####Listas:

rhttps://www.evernote.design/

www.uxlinks.com.br

www.sidebar.io

http://mail.asis.org/mailman/listinfo/sigia-l

http://www.ixda.org

https://groups.google.com/forum/#!forum/desinterac

http://lists.ibiblio.org/mailman/listinfo/aifia-pt

https://groups.google.com/forum/#!forum/acessodigital



####Cursos:

https://awari.com.br/ (online)

http://www.mergo.com.br/curso-ux-weekend.html

http://www.mergo.com.br/curso-acessibilidade-ux.html

http://www.mergo.com.br/curso-ux-metrics.html

https://www.sympla.com.br/formacao-em-ux-design__276365



####Podcast:

https://benetti.me/posts/podcasts/

https://podcasts.apple.com/br/podcast/awari-25-ux/id1387904528

https://anchor.fm/expatria

https://www.movimentoux.com/



####magens:

https://unsplash.com (pra mim, o melhor)

https://www.pexels.com (também é muito bom)

https://www.nappy.co (só de pessoas negras)

https://littlevisuals.co

http://thestocks.im (junta vários bancos diferentes)

https://pixabay.com

https://www.freepik.com/


Seguem algumas sugestões de leitura e capacitação.




**No Banco do Brasil:** 

[https://ux.bb.com.br/](https://mcas-proxyweb.us2.cas.ms/certificate-checker?login=false&originalUrl=https%3A%2F%2Fux.bb.com.br.us2.cas.ms%2F)

[https://mensagens.labbs.com.br/](https://mcas-proxyweb.us2.cas.ms/certificate-checker?login=false&originalUrl=https%3A%2F%2Fmensagens.labbs.com.br.us2.cas.ms%2F)

[https://cafecomux.labbs.com.br/](https://mcas-proxyweb.us2.cas.ms/certificate-checker?login=false&originalUrl=https%3A%2F%2Fcafecomux.labbs.com.br.us2.cas.ms%2F)

[http://uxconfbb.labbs.com.br/](https://mcas-proxyweb.us2.cas.ms/certificate-checker?login=false&originalUrl=http%3A%2F%2Fuxconfbb.labbs.com.br.us2.cas.ms%2F)

**Guia de Iniciantes:** 

[https://uxresources.design/](https://uxresources.design/)

# Editor.md

![](https://pandao.github.io/editor.md/images/logos/editormd-logo-180x180.png)

![](https://img.shields.io/github/stars/pandao/editor.md.svg) ![](https://img.shields.io/github/forks/pandao/editor.md.svg) ![](https://img.shields.io/github/tag/pandao/editor.md.svg) ![](https://img.shields.io/github/release/pandao/editor.md.svg) ![](https://img.shields.io/github/issues/pandao/editor.md.svg) ![](https://img.shields.io/bower/v/editor.md.svg)


**Table of Contents**

[TOCM]

[TOC]

#H1 headerSeguem algumas sugestões de leitura e capacitação.

[https://
##H2 header
###H3 header
####H4 header
#####H5 header
######H6 header
#Heading 1 link [Heading link](https://github.com/pandao/editor.md "Heading link")
##Heading 2 link [Heading link](https://github.com/pandao/editor.md "Heading link")
###Heading 3 link [Heading link](https://github.com/pandao/editor.md "Heading link")
####Heading 4 link [Heading link](https://github.com/pandao/editor.md "Heading link") Heading link [Heading link](https://github.com/pandao/editor.md "Heading link")
#####Heading 5 link [Heading link](https://github.com/pandao/editor.md "Heading link")
######Heading 6 link [Heading link](https://github.com/pandao/editor.md "Heading link")

##Headers (Underline)

H1 Header (Underline)
=============

H2 Header (Underline)
-------------

###Characters
                

----

~~Strikethrough~~ <s>Strikethrough (when enable html tag decode.)</s>
*Italic*      _Italic_
**Emphasis**  __Emphasis__
***Emphasis Italic*** ___Emphasis Italic___

Superscript: X<sub>2</sub>，Subscript: O<sup>2</sup>

**Abbreviation(link HTML abbr tag)**

The <abbr title="Hyper Text Markup Language">HTML</abbr> specification is maintained by the <abbr title="World Wide Web Consortium">W3C</abbr>.

###Blockquotes

> Blockquotes

Paragraphs and Line Breaks
                    
> "Blockquotes Blockquotes", [Link](http://localhost/)。

###Links

[Links](http://localhost/)

[Links with title](http://localhost/ "link title")

`<link>` : <https://github.com>

[Reference link][id/name] 

[id/name]: http://link-url/

GFM a-tail link @pandao

###Code Blocks (multi-language) & highlighting

####Inline code

`$ npm install marked`

####Code Blocks (Indented style)

Indented 4 spaces, like `<pre>` (Preformatted Text).

    <?php
        echo "Hello world!";
    ?>

Code Blocks (Preformatted text):

    | First Header  | Second Header |
    | ------------- | ------------- |
    | Content Cell  | Content Cell  |
    | Content Cell  | Content Cell  |

####Javascript　

```javascript
function test(){
	console.log("Hello world!");
}
 
(function(){
    var box = function(){
        return box.fn.init();
    };

    box.prototype = box.fn = {
        init : function(){
            console.log('box.init()');

			return this;
        },

		add : function(str){
			alert("add", str);

			return this;
		},

		remove : function(str){
			alert("remove", str);

			return this;
		}
    };
    
    box.fn.init.prototype = box.fn;
    
    window.box =box;
})();

var testBox = box();
testBox.add("jQuery").remove("jQuery");
```

####HTML code

```html
<!DOCTYPE html>
<html>
    <head>
        <mate charest="utf-8" />
        <title>Hello world!</title>
    </head>
    <body>
        <h1>Hello world!</h1>
    </body>
</html>
```

###Images

Image:

![](https://pandao.github.io/editor.md/examples/images/4.jpg)

> Follow your heart.

![](https://pandao.github.io/editor.md/examples/images/8.jpg)

> 图为：厦门白城沙滩 Xiamen

图片加链接 (Image + Link)：

[![](https://pandao.github.io/editor.md/examples/images/7.jpg)](https://pandao.github.io/editor.md/examples/images/7.jpg "李健首张专辑《似水流年》封面")

> 图为：李健首张专辑《似水流年》封面

----

###Lists

####Unordered list (-)

- Item A
- Item B
- Item C
  

####Unordered list (*)

* Item A
* Item B
* Item C

####Unordered list (plus sign and nested)
                
+ Item A
+ Item B
    + Item B 1
    + Item B 2
    + Item B 3
+ Item C
    * Item C 1
    * Item C 2
    * Item C 3

####Ordered list
                
1. Item A
2. Item B
3. Item C
                
----

###Tables
                    
| First Header | Second Header |
| ------------ | ------------- |
| Content Cell | Content Cell  |
| Content Cell | Content Cell  |

| First Header | Second Header |
| ------------ | ------------- |
| Content Cell | Content Cell  |
| Content Cell | Content Cell  |

| Function name | Description                |
| ------------- | -------------------------- |
| `help()`      | Display the help window.   |
| `destroy()`   | **Destroy your computer!** |

| Item     | Value |
| -------- | ----: |
| Computer | $1600 |
| Phone    |   $12 |
| Pipe     |    $1 |

| Left-Aligned  | Center Aligned  | Right Aligned |
| :------------ | :-------------: | ------------: |
| col 3 is      | some wordy text |         $1600 |
| col 2 is      |    centered     |           $12 |
| zebra stripes |    are neat     |            $1 |

----

####HTML entities

&copy; &  &uml; &trade; &iexcl; &pound;
&amp; &lt; &gt; &yen; &euro; &reg; &plusmn; &para; &sect; &brvbar; &macr; &laquo; &middot; 

X&sup2; Y&sup3; &frac34; &frac14;  &times;  &divide;   &raquo;

18&ordm;C  &quot;  &apos;

##Escaping for Special Characters

\*literal asterisks\*

##Markdown extras

###GFM task list

- [x] GFM task list 1
- [x] GFM task list 2
- [ ] GFM task list 3
    - [ ] GFM task list 3-1
    - [ ] GFM task list 3-2
    - [ ] GFM task list 3-3
- [ ] GFM task list 4
    - [ ] GFM task list 4-1
    - [ ] GFM task list 4-2

###Emoji mixed :smiley:

> Blockquotes :star:

####GFM task lists & Emoji & fontAwesome icon emoji & editormd logo emoji :editormd-logo-5x:

- [x] :smiley: @mentions, :smiley: #refs, [links](), **formatting**, and <del>tags</del> supported :editormd-logo:;
- [x] list syntax required (any unordered or ordered list supported) :editormd-logo-3x:;
- [x] [ ] :smiley: this is a complete item :smiley:;
- [ ] []this is an incomplete item [test link](#) :fa-star: @pandao; 
- [ ] [ ]this is an incomplete item :fa-star: :fa-gear:;
    - [ ] :smiley: this is an incomplete item [test link](#) :fa-star: :fa-gear:;
    - [ ] :smiley: this is  :fa-star: :fa-gear: an incomplete item [test link](#);
            

###TeX(LaTeX)

$$E=mc^2$$

Inline $$E=mc^2$$ Inline，Inline $$E=mc^2$$ Inline。

$$\(\sqrt{3x-1}+(1+x)^2\)$$
                    
$$\sin(\alpha)^{\theta}=\sum_{i=0}^{n}(x^i + \cos(f))$$
                
###FlowChart

```flow
st=>start: Login
op=>operation: Login operation
cond=>condition: Successful Yes or No?
e=>end: To admin

st->op->cond
cond(yes)->e
cond(no)->op
```

###Sequence Diagram
                    
```seq
Andrew->China: Says Hello 
Note right of China: China thinks\nabout it 
China-->Andrew: How are you? 
Andrew->>China: I am good thanks!
```

###End